<?php

namespace App\Event;

use Model\MongoDb\Event\Entity as EventEntity;

class Subscribe
{
    public function getListByEventType($eventType)
    {
        switch ($eventType)
        {
            case EventEntity::TYPE_PAYMENT:
                $result = array(Handler\Factory::HANDLER_CORE_PAYMENT);
                break;
            default:
            {
                throw new \Exception('Error. Invalid event type.' . $eventType);
            }
        }

        return $result;
    }
}