<?php

namespace App\Event;

use Model\MongoDb\Event\Entity as EventEntity;

interface HandlerInterface
{
    public function run(EventEntity $event);
}