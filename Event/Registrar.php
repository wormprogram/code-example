<?php

namespace App\Event;

use App\ServiceManager\Facade\Basic as BasicServiceManagerFacade;
use Model\MongoDb\Event\Entity as EventEntity;
use Model\MongoDb\Event\Mapper as EventMapper;
use App\Event\Exception as EventException;
use Model\MongoDb\Queue\Entity as QueueEntity;
use Model\MongoDb\Queue\Mapper as QueueMapper;
use Model\MongoDb\AdmitadUser\Mapper as AdmitadUserMapper;

class Registrar
{
    const SERVICE_NAME_CORE = 'core';

    /** @var  BasicServiceManagerFacade */
    private $serviceManager;

    public function __construct()
    {
        $this->serviceManager = \Zend_Registry::get('serviceManager');
    }

    public function handle($eventType, $data)
    {
        if (empty($data['user_id'])) {
            throw new EventException('Not found user_id in data');
        }

        if (!$this->isExistsAdmitadUserId($data['user_id'])) {
            return false;
        }

        $causeBill = new \App_Utils_Billing_Cause();
        if (!in_array($data['cause_bill'], $causeBill->getCauseShopotamServicePayment())) {
            return false;
        }

        $event = $this->addEvent($eventType, $data);
        $this->addEventHandlerList($eventType, $event);
        return true;
    }

    protected function addEvent($eventType, $data)
    {
        $data = $this->addAdmitadUserIdToData($data);

        $document = array(
            'type' => $eventType,
            'data' => $data,
            'created_date' => time(),
        );

        $event = new EventEntity();
        $event->setDataFromArray($document);
        $eventMapper = new EventMapper($this->serviceManager->getMongoClient());
        $eventResult = $eventMapper->save($event);
        return $eventResult;
    }

    private function addAdmitadUserIdToData($data)
    {
        $admitadUserMapper = new AdmitadUserMapper($this->serviceManager->getMongoClient());
        $admitadUser = $admitadUserMapper->findOneByUserId($data['user_id']);

        if ($admitadUser) {
            $data['admitad_user_id'] = $admitadUser->getAdmitadUserId();
            return $data;
        }
        return $data;
    }

    private function isExistsAdmitadUserId($userId)
    {
        $admitadUserMapper = new AdmitadUserMapper($this->serviceManager->getMongoClient());
        $admitadUser = $admitadUserMapper->findOneByUserId($userId);
        return (bool) $admitadUser;
    }

    protected function addEventHandlerList($eventType, EventEntity $event)
    {
        $queueMapper = new QueueMapper($this->serviceManager->getMongoClient());

        $eventSubscribe = new Subscribe();
        $eventSubscribeList = $eventSubscribe->getListByEventType($eventType);

        foreach ($eventSubscribeList as $handlerClassName) {
            $document = array(
                'name' => $this->getGeneratedQueueName($eventType, $event),
                'event_id' => $event->getId(),
                'handler' => $handlerClassName,
                'status' => QueueEntity::STATUS_NEW,
                'created_date' => time(),
                'modified_date' => null,
                'user_id' => $event->getUserId(),
            );

            $queue = new QueueEntity();
            $queue->setDataFromArray($document);
            $queueMapper->save($queue);
        }
    }

    /**
     * @param $eventType
     * @param \Model\MongoDb\Event\Entity $event
     * @return string - core.payment.123456 (например)
     * @throws \Exception
     */
    public function getGeneratedQueueName($eventType, EventEntity $event)
    {
        switch ($eventType)
        {
            case EventEntity::TYPE_PAYMENT:
            {
                $eventData = $event->getData();
                $queueName = self::SERVICE_NAME_CORE . '.' . QueueEntity::NAME_PAYMENT . '.' . $eventData['id'];
                break;
            }
            default:
            {
                throw new \Exception('Not found queue name for - ' . $eventType);
            }
        }

        return $queueName;
    }
}