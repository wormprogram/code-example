<?php

namespace App\Event;

use App\ServiceManager\Facade\Basic as BasicServiceManagerFacade;
use Model\MongoDb\Queue as Queue;
use Model\MongoDb\Mapper as Mapper;
use Model\MongoDb\Event as Event;
use App\Event\Exception as EventException;

class Controller
{
    private $serviceManager;

    public function __construct(BasicServiceManagerFacade $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    public function run()
    {
        $mongo = $this->serviceManager->getMongoClient();

        $queueMapper = new Queue\Mapper($mongo);
        /** @var $task Queue\Entity */
        $task = $queueMapper->findOne(array('status' => Queue\Entity::STATUS_NEW), array(Mapper::OPTION_SORT => array('_id' => 1)));
        
        // нет событий для обработки
        if (!$task) {
            return null;
        }

        $queueMapper->updateStatusByEntity($task, Queue\Entity::STATUS_BUSY);

        $eventMapper = new Event\Mapper($mongo);
        $event = $eventMapper->findOne(array('_id' => new \MongoId($task->getEventId())));

        if (!$event) {
            throw new EventException('Not found event for task');
        }

        $handler = $this->createHandler($task);

        try {
            $handler->run($event);
        } catch (EventException $e) {
            $queueMapper->updateStatusByEntity($task, Queue\Entity::STATUS_ERROR);
            $this->serviceManager->getSentryClient()->handleException($e);
        }

        $queueMapper->updateStatusByEntity($task, Queue\Entity::STATUS_DONE);
    }

    /**
     * @param Queue\Entity $task
     * @return HandlerInterface
     */
    protected function createHandler(Queue\Entity $task)
    {
        $handlerTypeName = $task->getHandler();
        $factory = new Handler\Factory($this->serviceManager);
        $handler = $factory->create($handlerTypeName);
        return $handler;
    }

    /**
     * @param $userId
     * @return \Model\MongoDb\Collection
     */
    public function getEventListByUserId($userId)
    {
        if (!$userId) {
            return false;
        }

        $mongo = \Database::getMongo();

        $queueMapper = new Queue\Mapper($mongo);
        $taskList = $queueMapper->find(
            array(
                'user_id' => $userId,
                'status' => Queue\Entity::STATUS_NEW,
            ),
            array(
                Mapper::OPTION_SORT => array('_id' => 1),
                Mapper::OPTION_LIMIT => 10,
            )
        );

        $eventMapper = new Event\Mapper($mongo);

        $eventIdList = array();

        /** @var $task Queue\Entity */
        foreach ($taskList as $task) {
            $eventIdList[] = $task->getEventId();
        }

        $eventList = $eventMapper->find(
            array(
                '_id' => array(
                    '$in' => $eventIdList
                ),
            )
        );

        return $eventList;
    }
}