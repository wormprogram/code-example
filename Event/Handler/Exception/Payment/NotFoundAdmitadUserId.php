<?php

namespace App\Event\Handler\Exception\Payment;

use App\Event\Exception as EventException;

class NotFoundAdmitadUserId extends EventException {}