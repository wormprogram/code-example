<?php

namespace App\Event\Handler;

use App\ServiceManager\Facade\Basic as BasicServiceManagerFacade;

class Factory
{
    const HANDLER_CORE_PAYMENT = 'core_event_handler_payment';

    private $serviceManager;

    public function __construct(BasicServiceManagerFacade $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    public function create($handlerTypeName)
    {
        switch ($handlerTypeName)
        {
            case self::HANDLER_CORE_PAYMENT:
            {
                $handler = new Payment($this->serviceManager);
                break;
            }
            default:
            {
                throw new \Exception('Not found event handler for - ' . $handlerTypeName);
            }
        }

        return $handler;
    }
}