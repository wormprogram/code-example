<?php

namespace App\Event\Handler;

use App\ServiceManager\Facade\Basic as BasicServiceManagerFacade;
use App\Event as EventHandler;
use Model\MongoDb\Event as MongoDbEvent;
use App\Event\Handler\Exception\Payment as PaymentException;

class Payment implements EventHandler\HandlerInterface
{
    private $serviceManager;

    public function __construct(BasicServiceManagerFacade $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    public function run(MongoDbEvent\Entity $event)
    {
        $requestUrl = $this->getCallbackUrl($event, \Convert::getInstance());
        $httpClient = new \Zend_Http_Client($requestUrl);
        $httpClient->setConfig(array('adapter'=>'Zend_Http_Client_Adapter_Curl','maxredirects' => 0,'timeout'=> 3));
        $response = $httpClient->request();
        $this->log($httpClient, $event->getData());
        $this->checkResponse($response);
    }

    public function getCallbackUrl(MongoDbEvent\Entity $event, \Convert $moneyConverter)
    {
        $eventData = $event->getData();
        $price = $moneyConverter->getConvertedAmountFromCurrencyToUsd($eventData['amount'] * -1, $eventData['currency']);
        $orderId = $this->getOrderIdFromEventData($eventData);
        $admitadUserId = $this->getAdmitadUserId($event);
        $serviceTypeId = $this->getAdmitadPaymentServiceTypeId($eventData);
        $uri = "https://ad.admitad.com/register/c44dsdk3452baf/script_type/img/payment_type/sale/product/" . $serviceTypeId .
            "/postback/1/cart/{$price}/order_id/{$orderId}/uid/{$admitadUserId}/";

        return $uri;
    }

    private function getOrderIdFromEventData($eventData)
    {
        $causeBill = new \App_Utils_Billing_Cause();
        return $causeBill->getCauseBillShortNameByCauseBill($eventData['cause_bill']) . $eventData['item_id'];
    }

    private function getAdmitadPaymentServiceTypeId($eventData)
    {
        $causeBill = new \App_Utils_Billing_Cause();
        return $causeBill->getShopotamServiceTypeIdByCauseBill($eventData['cause_bill']);
    }

    /**
     * @param MongoDbEvent\Entity $event
     * @return string
     * @throws Exception\Payment\NotFoundAdmitadUserId
     */
    private function getAdmitadUserId(MongoDbEvent\Entity $event)
    {
        $eventData = $event->getData();
        if (!isset($eventData['admitad_user_id'])) {
            throw new PaymentException\NotFoundAdmitadUserId('Not found admitad user id');
        }
        return $eventData['admitad_user_id'];
    }

    private function checkResponse(\Zend_Http_Response $response)
    {
        if (!$response->isSuccessful()) {
            throw new PaymentException\HttpConnectError('Http response error');
        }
    }

    private function log(\Zend_Http_Client $httpClient, $eventData)
    {
        $logData = array(
            'order_id' => $this->getOrderIdFromEventData($eventData),
            'status' => $httpClient->getLastResponse()->getStatus(),
            'message' => $httpClient->getLastResponse()->getMessage(),
            'headers' => $httpClient->getLastResponse()->getHeadersAsString(),
            'row_request' => $httpClient->getLastRequest(),
            'row_response' => base64_encode($httpClient->getLastResponse()->getRawBody())
        );

        $this->serviceManager->getLogger(\App\Logger::COLLECTION_NAME_ADMITAD_CALL)
            ->record(
                \App\Logger::LVL_DEBUG,
                'admitad',
                $logData
            );
    }
}